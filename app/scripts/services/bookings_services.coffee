'use strict'

app = angular.module('taxiHomeApp')

# Synchronous communication  (CRUD interface)
app.service 'BookingsService', ($resource) ->
  $resource 'http://localhost:3000/orders', {}
  #$resource 'http://172.19.0.129:3000/orders', {}
  #resource 'http://strs-team1.herokuapp.com/orders', {}


# Asynchronous communication (via Pusher)
app.service 'BookingsPusherService', ($rootScope) ->
  pusher = new Pusher('aee2891b234084cfc01e')
  channel = pusher.subscribe('taxiHome')
  message = ''

  channel.bind 'async_notification', (data) ->
    message = data.message
    $rootScope.$broadcast 'message-received'

  getMessage: ->
    message