'use strict'

app = angular.module('taxiHomeApp')

app.factory 'Framework', ($q) ->
  _navigator = $q.defer()
  _cordova = $q.defer()

  unless window.cordova?
    _navigator.resolve window.navigator
    _cordova.resolve false
  else
    alert "it is a cordova based system"
    document.addEventListener 'deviceready', (evt) ->
      _navigator.resolve navigator
      _cordova.resolve true
  return {
    navigator: -> _navigator.promise
    cordova: -> _cordova.promise
  }