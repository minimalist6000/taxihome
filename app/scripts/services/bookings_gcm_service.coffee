'use strict'

app = angular.module('taxiHomeApp')
app.service 'BookingsGCMService', ($rootScope, Framework) ->
  regId = undefined
  onMessageCallback = undefined
  message = ''
  Framework.cordova().then (isCordova) ->
    if isCordova
      onSuccess = (result) ->
        alert "GCM connected: #{result}"
      onError = (error) ->
        alert "GCM error: #{error}"

      window.onNotificationGCM = (notification) ->
        switch notification.event
          when "registered"
            regId = notification.regid
            alert "#{JSON.stringify(notification)}"
            alert "RegID: #{regId}"

          when "message"
            message = notification.message
            $rootScope.$apply ->
              onMessageCallback(notification.message)
          when "error"
            alert "Error while receiving GCM push notification: #{notification}"
          else
            alert "Unknown GCM push notification received: #{notification}"

      window.plugins.pushNotification
        .register(onSuccess, onError,{"senderID":"789067696452","ecb":"onNotificationGCM"})
  return {
    onMessage: (callback) ->
      onMessageCallback = callback
    deviceId: ->
      regId
    getMessage: ->
      message
  }