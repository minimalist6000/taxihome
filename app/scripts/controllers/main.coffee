'use strict'

app = angular.module('taxiHomeApp')

ModalInstanceCtrl = ($scope, $modalInstance, address, name, phone, order_id) ->
  $scope.address  = address
  $scope.name     = name
  $scope.phone    = phone
  $scope.order_id = order_id
  $scope.ok = ->
    $modalInstance.close($scope.order_id)

  $scope.cancel = ->
    $modalInstance.dismiss($scope.order_id)
#    result = 'N_' + $scope.order_id

#    $modalInstance.close(result)

app.controller 'MainCtrl', ($scope, $injector, $modal, $http, Framework, BookingsService) ->
  $scope.sync_notification = ''
  $scope.async_notification = ''

  $scope.map = {}
  $scope.marker = {}
  $scope.latitude = 0
  $scope.longitude = 0

  Framework.navigator().then (navigator) ->
    navigator.geolocation.getCurrentPosition (position) ->
      longitude = position.coords.longitude
      latitude = position.coords.latitude
      $scope.$apply ->
        $scope.latitude = latitude
        $scope.longitude = longitude
      mapProp =
        center: new google.maps.LatLng(latitude, longitude)
        zoom: 16
        mapTypeId: google.maps.MapTypeId.ROADMAP
      $scope.map = new google.maps.Map(document.getElementById('map_canvas'), mapProp)
      $scope.marker = new google.maps.Marker
        map: $scope.map,
        position: mapProp.center

  BookingsAsyncService = {}
  device_id = BookingsAsyncService.deviceId() if BookingsAsyncService.deviceId?
  $scope.submit = ->
    request = {start_location: 'Liivi 2, Tartu, Estonia'}

    request['device_id'] = BookingsAsyncService.deviceId() if BookingsAsyncService.deviceId?
    BookingsService.save(request, (response) ->
      $scope.sync_notification = response.message
    )

  #Framework.cordova().then (isCordova) ->
  #  if isCordova
  #    BookingsAsyncService = $injector.get('BookingsGCMService')
  #  else
  #    BookingsAsyncService = $injector.get('BookingsPusherService')
  #  BookingsAsyncService.onMessage (data) ->
  #    $scope.async_notification = data

  BookingsAsyncService = $injector.get('BookingsPusherService')

  $scope.$on 'message-received', ->
    #$scope.async_notification = BookingsAsyncService.getMessage()
    #$scope.$digest()
    messageFromBackend = angular.fromJson(BookingsAsyncService.getMessage())
    address  = messageFromBackend.address
    name     = messageFromBackend.name
    phone    = messageFromBackend.phone
    order_id = messageFromBackend.order_id

    modalInstance = $modal.open
      templateUrl: 'views/myModalContent.html',
      controller: ModalInstanceCtrl,
      size: 100,
      resolve: 
        address: ->
          address
        name: ->
          name
        phone: ->
          phone
        order_id: ->
          order_id

    modalInstance.result.then (result) -> #close
      #alert result
      #url  = 'http://strs-team1.herokuapp.com/orders/' + result + '/accept'
      url  = 'http://localhost:3000/orders/' + result + '/accept'

      data = ''
      $http.post(url, data)
    , (result) -> #dismiss
      #alert result
      #url  = 'http://strs-team1.herokuapp.com/orders/' + result + '/reject'
      url  = 'http://localhost:3000/orders/' + result + '/reject'

      data = ''
      $http.post(url, data)




  $scope.popup = ->
    modalInstance = $modal.open
      templateUrl: 'views/myModalContent.html',
      controller: ModalInstanceCtrl,
      size: 100,
      resolve: 
        message: ->
          "Hello popup"
